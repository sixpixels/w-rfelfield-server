package wurfelfield;

import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;

public class ServerListener implements MessageListener<HostedConnection> 
{
    float firstx, firsty, firstz;
    float firstrx, firstry, firstrz;
    int firstlife;
    float secondx, secondy, secondz;
    float secondrx, secondry, secondrz;
    int secondlife;
    public ServerListener(Player p1, Player p2)
    {
        firstx = p1.playerc.getPhysicsLocation().x;
        firsty = p1.playerc.getPhysicsLocation().y;
        firstz = p1.playerc.getPhysicsLocation().z;
        firstrx = p1.playerc.getViewDirection().getX();
        firstry = p1.playerc.getViewDirection().getY();
        firstrz = p1.playerc.getViewDirection().getZ();
        firstlife = p1.life;
        secondx = p2.playerc.getPhysicsLocation().x;
        secondy = p2.playerc.getPhysicsLocation().y;
        secondz = p2.playerc.getPhysicsLocation().z;
        secondrx = p2.playerc.getViewDirection().getX();
        secondry = p2.playerc.getViewDirection().getY();
        secondrz = p2.playerc.getViewDirection().getZ();
        secondlife = p2.life;
    }
  public void messageReceived(HostedConnection source, Message message) {
    if (message instanceof StatusMessage) {
      // do something with the message
      StatusMessage msg = (StatusMessage) message;
      try{
      if(source.getId() == source.getServer().getConnection(0).getId())
      {
          firstx = msg.x;
          firsty = msg.y;
          firstz = msg.z;
          firstrx = msg.rx;
          firstry = msg.ry;
          firstrz = msg.rz;
          secondlife = msg.life;
      }
      if(source.getId() == source.getServer().getConnection(1).getId())
      {
          secondx = msg.x;
          secondy = msg.y;
          secondz = msg.z;
          secondrx = msg.rx;
          secondry = msg.ry;
          secondrz = msg.rz;
          firstlife = msg.life;
      }
    } // else....
    catch(Exception e)
    {
        e.printStackTrace();
    }
    }
  }
}