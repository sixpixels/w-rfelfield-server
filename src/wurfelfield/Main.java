package wurfelfield;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.network.Filters;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.network.serializing.Serializer;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.system.JmeContext;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Main extends SimpleApplication {
    
            Server myServer;
            ServerListener sl;
  public static void main(String[] args) {
      String ip = "";
    try {
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface iface = interfaces.nextElement();
            // filters out 127.0.0.1 and inactive interfaces
            if (iface.isLoopback() || !iface.isUp())
                continue;

            Enumeration<InetAddress> addresses = iface.getInetAddresses();
            while(addresses.hasMoreElements()) {
                InetAddress addr = addresses.nextElement();
                ip = addr.getHostAddress();
            }
        }
    } catch (SocketException e) {
        throw new RuntimeException(e);
    }
    //JOptionPane.showMessageDialog(null, "Deine IP: " + ip);
    System.out.println(ip);
    Main app = new Main();
    app.start(JmeContext.Type.Headless); // headless type for servers!
  }
    @Override
    public void simpleInitApp() {
        try {
            Serializer.registerClass(StatusMessage.class);
            myServer = Network.createServer(6143);
            myServer.start();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void simpleUpdate(float tpf) {
        if(myServer.getConnections().size() == 2)
        {
            if(sl == null)
            {
                System.out.println(" SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs");
                sl = new ServerListener(new Player(rootNode, assetManager, new Vector3f(0, 10, 0)),
                        new Player(rootNode, assetManager, new Vector3f(301, 10, 0)));
                myServer.addMessageListener(sl, StatusMessage.class);
            }
            myServer.broadcast(Filters.in(myServer.getConnection(0)), new StatusMessage(sl.secondx, sl.secondy, sl.secondz, sl.secondrx, sl.secondry, sl.secondrz, sl.firstlife));
            myServer.broadcast(Filters.in(myServer.getConnection(1)), new StatusMessage(sl.firstx, sl.firsty, sl.firstz, sl.firstrx, sl.firstry, sl.firstrz, sl.secondlife));
      
        }
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
}
