/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wurfelfield;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author simon
 */
@Serializable
public class StatusMessage extends AbstractMessage {
    //Player main;
    //Player opponent;
    float x,y,z, rx, ry, rz;
    int life;
    public StatusMessage(Player m, Player o) 
    {
        x = m.playerc.getPhysicsLocation().x;
        y = m.playerc.getPhysicsLocation().y;
        z = m.playerc.getPhysicsLocation().z;
        rx = m.playerc.getViewDirection().getX();
        ry = m.playerc.getViewDirection().getY();
        rz = m.playerc.getViewDirection().getZ();
        life = o.life;
    }
    public StatusMessage(float mx, float my, float mz, float mrx, float mry, float mrz, int olife) 
    {
        x = mx;
        y = my;
        z = mz;
        rx = mrx;
        ry = mry;
        rz = mrz;
        life = olife;
        System.out.println("x: " + mrx + " y: " + mry + "z: " + mrz + " life : " + olife);
    } // custom constructor 
    public StatusMessage() 
    {
    } // custom constructor
}
